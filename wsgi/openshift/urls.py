from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'openshift.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'views.home', name='home'),
    (r'^records/', include('sumatra_server.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
