SET sql_mode='NO_BACKSLASH_ESCAPES';
CREATE TABLE `django_admin_log` (
    `id` integer NOT NULL PRIMARY KEY,
    `action_time` datetime NOT NULL,
    `user_id` integer NOT NULL,
    `content_type_id` integer,
    `object_id` text,
    `object_repr` varchar(200) NOT NULL,
    `action_flag` smallint unsigned NOT NULL,
    `change_message` text NOT NULL
);
CREATE TABLE `auth_permission` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(50) NOT NULL,
    `content_type_id` integer NOT NULL,
    `codename` varchar(100) NOT NULL,
    UNIQUE (`content_type_id`, `codename`)
);
INSERT INTO `auth_permission` VALUES(1,'Can add log entry',1,'add_logentry');
INSERT INTO `auth_permission` VALUES(2,'Can change log entry',1,'change_logentry');
INSERT INTO `auth_permission` VALUES(3,'Can delete log entry',1,'delete_logentry');
INSERT INTO `auth_permission` VALUES(4,'Can add permission',2,'add_permission');
INSERT INTO `auth_permission` VALUES(5,'Can change permission',2,'change_permission');
INSERT INTO `auth_permission` VALUES(6,'Can delete permission',2,'delete_permission');
INSERT INTO `auth_permission` VALUES(7,'Can add group',3,'add_group');
INSERT INTO `auth_permission` VALUES(8,'Can change group',3,'change_group');
INSERT INTO `auth_permission` VALUES(9,'Can delete group',3,'delete_group');
INSERT INTO `auth_permission` VALUES(10,'Can add user',4,'add_user');
INSERT INTO `auth_permission` VALUES(11,'Can change user',4,'change_user');
INSERT INTO `auth_permission` VALUES(12,'Can delete user',4,'delete_user');
INSERT INTO `auth_permission` VALUES(13,'Can add content type',5,'add_contenttype');
INSERT INTO `auth_permission` VALUES(14,'Can change content type',5,'change_contenttype');
INSERT INTO `auth_permission` VALUES(15,'Can delete content type',5,'delete_contenttype');
INSERT INTO `auth_permission` VALUES(16,'Can add session',6,'add_session');
INSERT INTO `auth_permission` VALUES(17,'Can change session',6,'change_session');
INSERT INTO `auth_permission` VALUES(18,'Can delete session',6,'delete_session');
INSERT INTO `auth_permission` VALUES(19,'Can add migration history',7,'add_migrationhistory');
INSERT INTO `auth_permission` VALUES(20,'Can change migration history',7,'change_migrationhistory');
INSERT INTO `auth_permission` VALUES(21,'Can delete migration history',7,'delete_migrationhistory');
INSERT INTO `auth_permission` VALUES(22,'Can add project permission',8,'add_projectpermission');
INSERT INTO `auth_permission` VALUES(23,'Can change project permission',8,'change_projectpermission');
INSERT INTO `auth_permission` VALUES(24,'Can delete project permission',8,'delete_projectpermission');
INSERT INTO `auth_permission` VALUES(25,'Can add project',9,'add_project');
INSERT INTO `auth_permission` VALUES(26,'Can change project',9,'change_project');
INSERT INTO `auth_permission` VALUES(27,'Can delete project',9,'delete_project');
INSERT INTO `auth_permission` VALUES(28,'Can add executable',10,'add_executable');
INSERT INTO `auth_permission` VALUES(29,'Can change executable',10,'change_executable');
INSERT INTO `auth_permission` VALUES(30,'Can delete executable',10,'delete_executable');
INSERT INTO `auth_permission` VALUES(31,'Can add dependency',11,'add_dependency');
INSERT INTO `auth_permission` VALUES(32,'Can change dependency',11,'change_dependency');
INSERT INTO `auth_permission` VALUES(33,'Can delete dependency',11,'delete_dependency');
INSERT INTO `auth_permission` VALUES(34,'Can add repository',12,'add_repository');
INSERT INTO `auth_permission` VALUES(35,'Can change repository',12,'change_repository');
INSERT INTO `auth_permission` VALUES(36,'Can delete repository',12,'delete_repository');
INSERT INTO `auth_permission` VALUES(37,'Can add parameter set',13,'add_parameterset');
INSERT INTO `auth_permission` VALUES(38,'Can change parameter set',13,'change_parameterset');
INSERT INTO `auth_permission` VALUES(39,'Can delete parameter set',13,'delete_parameterset');
INSERT INTO `auth_permission` VALUES(40,'Can add launch mode',14,'add_launchmode');
INSERT INTO `auth_permission` VALUES(41,'Can change launch mode',14,'change_launchmode');
INSERT INTO `auth_permission` VALUES(42,'Can delete launch mode',14,'delete_launchmode');
INSERT INTO `auth_permission` VALUES(43,'Can add datastore',15,'add_datastore');
INSERT INTO `auth_permission` VALUES(44,'Can change datastore',15,'change_datastore');
INSERT INTO `auth_permission` VALUES(45,'Can delete datastore',15,'delete_datastore');
INSERT INTO `auth_permission` VALUES(46,'Can add data key',16,'add_datakey');
INSERT INTO `auth_permission` VALUES(47,'Can change data key',16,'change_datakey');
INSERT INTO `auth_permission` VALUES(48,'Can delete data key',16,'delete_datakey');
INSERT INTO `auth_permission` VALUES(49,'Can add platform information',17,'add_platforminformation');
INSERT INTO `auth_permission` VALUES(50,'Can change platform information',17,'change_platforminformation');
INSERT INTO `auth_permission` VALUES(51,'Can delete platform information',17,'delete_platforminformation');
INSERT INTO `auth_permission` VALUES(52,'Can add record',18,'add_record');
INSERT INTO `auth_permission` VALUES(53,'Can change record',18,'change_record');
INSERT INTO `auth_permission` VALUES(54,'Can delete record',18,'delete_record');
INSERT INTO `auth_permission` VALUES(55,'Can add tag',19,'add_tag');
INSERT INTO `auth_permission` VALUES(56,'Can change tag',19,'change_tag');
INSERT INTO `auth_permission` VALUES(57,'Can delete tag',19,'delete_tag');
INSERT INTO `auth_permission` VALUES(58,'Can add tagged item',20,'add_taggeditem');
INSERT INTO `auth_permission` VALUES(59,'Can change tagged item',20,'change_taggeditem');
INSERT INTO `auth_permission` VALUES(60,'Can delete tagged item',20,'delete_taggeditem');
CREATE TABLE `auth_group_permissions` (
    `id` integer NOT NULL PRIMARY KEY,
    `group_id` integer NOT NULL,
    `permission_id` integer NOT NULL REFERENCES `auth_permission` (`id`),
    UNIQUE (`group_id`, `permission_id`)
);
CREATE TABLE `auth_group` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(80) NOT NULL UNIQUE
);
CREATE TABLE `auth_user_groups` (
    `id` integer NOT NULL PRIMARY KEY,
    `user_id` integer NOT NULL,
    `group_id` integer NOT NULL REFERENCES `auth_group` (`id`),
    UNIQUE (`user_id`, `group_id`)
);
CREATE TABLE `auth_user_user_permissions` (
    `id` integer NOT NULL PRIMARY KEY,
    `user_id` integer NOT NULL,
    `permission_id` integer NOT NULL REFERENCES `auth_permission` (`id`),
    UNIQUE (`user_id`, `permission_id`)
);
CREATE TABLE `auth_user` (
    `id` integer NOT NULL PRIMARY KEY,
    `password` varchar(128) NOT NULL,
    `last_login` datetime NOT NULL,
    `is_superuser` bool NOT NULL,
    `username` varchar(30) NOT NULL UNIQUE,
    `first_name` varchar(30) NOT NULL,
    `last_name` varchar(30) NOT NULL,
    `email` varchar(75) NOT NULL,
    `is_staff` bool NOT NULL,
    `is_active` bool NOT NULL,
    `date_joined` datetime NOT NULL
);
INSERT INTO `auth_user` VALUES(1,'pbkdf2_sha256$12000$FzQiLS3GSY1T$hCXnLWIQQdiyHRdxdVV/Ap9yIkLws6Z3AoGKypIEiCY=','2014-05-26 15:31:39.640431',1,'admin','','','nobody@nobody.com',1,1,'2014-05-26 13:51:13.647663');
CREATE TABLE `django_content_type` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(100) NOT NULL,
    `app_label` varchar(100) NOT NULL,
    `model` varchar(100) NOT NULL,
    UNIQUE (`app_label`, `model`)
);
INSERT INTO `django_content_type` VALUES(1,'log entry','admin','logentry');
INSERT INTO `django_content_type` VALUES(2,'permission','auth','permission');
INSERT INTO `django_content_type` VALUES(3,'group','auth','group');
INSERT INTO `django_content_type` VALUES(4,'user','auth','user');
INSERT INTO `django_content_type` VALUES(5,'content type','contenttypes','contenttype');
INSERT INTO `django_content_type` VALUES(6,'session','sessions','session');
INSERT INTO `django_content_type` VALUES(7,'migration history','south','migrationhistory');
INSERT INTO `django_content_type` VALUES(8,'project permission','sumatra_server','projectpermission');
INSERT INTO `django_content_type` VALUES(9,'project','django_store','project');
INSERT INTO `django_content_type` VALUES(10,'executable','django_store','executable');
INSERT INTO `django_content_type` VALUES(11,'dependency','django_store','dependency');
INSERT INTO `django_content_type` VALUES(12,'repository','django_store','repository');
INSERT INTO `django_content_type` VALUES(13,'parameter set','django_store','parameterset');
INSERT INTO `django_content_type` VALUES(14,'launch mode','django_store','launchmode');
INSERT INTO `django_content_type` VALUES(15,'datastore','django_store','datastore');
INSERT INTO `django_content_type` VALUES(16,'data key','django_store','datakey');
INSERT INTO `django_content_type` VALUES(17,'platform information','django_store','platforminformation');
INSERT INTO `django_content_type` VALUES(18,'record','django_store','record');
INSERT INTO `django_content_type` VALUES(19,'tag','tagging','tag');
INSERT INTO `django_content_type` VALUES(20,'tagged item','tagging','taggeditem');
CREATE TABLE `django_session` (
    `session_key` varchar(40) NOT NULL PRIMARY KEY,
    `session_data` text NOT NULL,
    `expire_date` datetime NOT NULL
);
CREATE TABLE `south_migrationhistory` (
    `id` integer NOT NULL PRIMARY KEY,
    `app_name` varchar(255) NOT NULL,
    `migration` varchar(255) NOT NULL,
    `applied` datetime NOT NULL
);
CREATE TABLE `sumatra_server_projectpermission` (
    `id` integer NOT NULL PRIMARY KEY,
    `user_id` integer NOT NULL REFERENCES `auth_user` (`id`),
    `project_id` varchar(50) NOT NULL
);
CREATE TABLE `django_store_project` (
    `id` varchar(50) NOT NULL PRIMARY KEY,
    `name` varchar(200) NOT NULL,
    `description` text NOT NULL
);
CREATE TABLE `django_store_executable` (
    `id` integer NOT NULL PRIMARY KEY,
    `path` varchar(200) NOT NULL,
    `name` varchar(50) NOT NULL,
    `version` varchar(100) NOT NULL,
    `options` varchar(50) NOT NULL
);
CREATE TABLE `django_store_dependency` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(50) NOT NULL,
    `path` varchar(200) NOT NULL,
    `version` varchar(20) NOT NULL,
    `diff` text NOT NULL,
    `source` varchar(200),
    `module` varchar(50) NOT NULL
);
CREATE TABLE `django_store_repository` (
    `id` integer NOT NULL PRIMARY KEY,
    `type` varchar(100) NOT NULL,
    `url` varchar(200) NOT NULL,
    `upstream` varchar(200)
);
CREATE TABLE `django_store_parameterset` (
    `id` integer NOT NULL PRIMARY KEY,
    `type` varchar(100) NOT NULL,
    `content` text NOT NULL
);
CREATE TABLE `django_store_launchmode` (
    `id` integer NOT NULL PRIMARY KEY,
    `type` varchar(100) NOT NULL,
    `parameters` varchar(1000) NOT NULL
);
CREATE TABLE `django_store_datastore` (
    `id` integer NOT NULL PRIMARY KEY,
    `type` varchar(100) NOT NULL,
    `parameters` varchar(200) NOT NULL
);
CREATE TABLE `django_store_datakey` (
    `id` integer NOT NULL PRIMARY KEY,
    `path` varchar(200) NOT NULL,
    `digest` varchar(40) NOT NULL,
    `metadata` text NOT NULL
);
CREATE TABLE `django_store_platforminformation` (
    `id` integer NOT NULL PRIMARY KEY,
    `architecture_bits` varchar(100) NOT NULL,
    `architecture_linkage` varchar(100) NOT NULL,
    `machine` varchar(20) NOT NULL,
    `network_name` varchar(100) NOT NULL,
    `ip_addr` char(15) NOT NULL,
    `processor` varchar(100) NOT NULL,
    `release` varchar(100) NOT NULL,
    `system_name` varchar(20) NOT NULL,
    `version` varchar(100) NOT NULL
);
CREATE TABLE `django_store_record_platforms` (
    `id` integer NOT NULL PRIMARY KEY,
    `record_id` integer NOT NULL,
    `platforminformation_id` integer NOT NULL REFERENCES `django_store_platforminformation` (`id`),
    UNIQUE (`record_id`, `platforminformation_id`)
);
CREATE TABLE `django_store_record_dependencies` (
    `id` integer NOT NULL PRIMARY KEY,
    `record_id` integer NOT NULL,
    `dependency_id` integer NOT NULL REFERENCES `django_store_dependency` (`id`),
    UNIQUE (`record_id`, `dependency_id`)
);
CREATE TABLE `django_store_record_input_data` (
    `id` integer NOT NULL PRIMARY KEY,
    `record_id` integer NOT NULL,
    `datakey_id` integer NOT NULL REFERENCES `django_store_datakey` (`id`),
    UNIQUE (`record_id`, `datakey_id`)
);
CREATE TABLE `django_store_record_output_data` (
    `id` integer NOT NULL PRIMARY KEY,
    `record_id` integer NOT NULL,
    `datakey_id` integer NOT NULL REFERENCES `django_store_datakey` (`id`),
    UNIQUE (`record_id`, `datakey_id`)
);
CREATE TABLE `django_store_record` (
    `label` varchar(100) NOT NULL,
    `db_id` integer NOT NULL PRIMARY KEY,
    `reason` text NOT NULL,
    `duration` real,
    `executable_id` integer REFERENCES `django_store_executable` (`id`),
    `repository_id` integer REFERENCES `django_store_repository` (`id`),
    `main_file` varchar(100) NOT NULL,
    `version` varchar(50) NOT NULL,
    `parameters_id` integer NOT NULL REFERENCES `django_store_parameterset` (`id`),
    `launch_mode_id` integer NOT NULL REFERENCES `django_store_launchmode` (`id`),
    `datastore_id` integer NOT NULL REFERENCES `django_store_datastore` (`id`),
    `input_datastore_id` integer NOT NULL REFERENCES `django_store_datastore` (`id`),
    `outcome` text NOT NULL,
    `timestamp` datetime NOT NULL,
    `tags` varchar(255) NOT NULL,
    `diff` text NOT NULL,
    `user` varchar(100) NOT NULL,
    `project_id` varchar(50) REFERENCES `django_store_project` (`id`),
    `script_arguments` text NOT NULL,
    `stdout_stderr` text NOT NULL,
    `repeats` varchar(100)
);
CREATE TABLE `tagging_tag` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(50) NOT NULL UNIQUE
);
CREATE TABLE `tagging_taggeditem` (
    `id` integer NOT NULL PRIMARY KEY,
    `tag_id` integer NOT NULL REFERENCES `tagging_tag` (`id`),
    `content_type_id` integer NOT NULL REFERENCES `django_content_type` (`id`),
    `object_id` integer unsigned NOT NULL,
    UNIQUE (`tag_id`, `content_type_id`, `object_id`)
);
CREATE INDEX `django_admin_log_6340c63c` ON `django_admin_log` (`user_id`);
CREATE INDEX `django_admin_log_37ef4eb4` ON `django_admin_log` (`content_type_id`);
CREATE INDEX `auth_permission_37ef4eb4` ON `auth_permission` (`content_type_id`);
CREATE INDEX `auth_group_permissions_5f412f9a` ON `auth_group_permissions` (`group_id`);
CREATE INDEX `auth_group_permissions_83d7f98b` ON `auth_group_permissions` (`permission_id`);
CREATE INDEX `auth_user_groups_6340c63c` ON `auth_user_groups` (`user_id`);
CREATE INDEX `auth_user_groups_5f412f9a` ON `auth_user_groups` (`group_id`);
CREATE INDEX `auth_user_user_permissions_6340c63c` ON `auth_user_user_permissions` (`user_id`);
CREATE INDEX `auth_user_user_permissions_83d7f98b` ON `auth_user_user_permissions` (`permission_id`);
CREATE INDEX `django_session_b7b81f0c` ON `django_session` (`expire_date`);
CREATE INDEX `sumatra_server_projectpermission_6340c63c` ON `sumatra_server_projectpermission` (`user_id`);
CREATE INDEX `sumatra_server_projectpermission_37952554` ON `sumatra_server_projectpermission` (`project_id`);
CREATE INDEX `django_store_record_platforms_52103e4c` ON `django_store_record_platforms` (`record_id`);
CREATE INDEX `django_store_record_platforms_163577c8` ON `django_store_record_platforms` (`platforminformation_id`);
CREATE INDEX `django_store_record_dependencies_52103e4c` ON `django_store_record_dependencies` (`record_id`);
CREATE INDEX `django_store_record_dependencies_06cb6e7d` ON `django_store_record_dependencies` (`dependency_id`);
CREATE INDEX `django_store_record_input_data_52103e4c` ON `django_store_record_input_data` (`record_id`);
CREATE INDEX `django_store_record_input_data_58bbd806` ON `django_store_record_input_data` (`datakey_id`);
CREATE INDEX `django_store_record_output_data_52103e4c` ON `django_store_record_output_data` (`record_id`);
CREATE INDEX `django_store_record_output_data_58bbd806` ON `django_store_record_output_data` (`datakey_id`);
CREATE INDEX `django_store_record_49fc9cd9` ON `django_store_record` (`executable_id`);
CREATE INDEX `django_store_record_a322ed1e` ON `django_store_record` (`repository_id`);
CREATE INDEX `django_store_record_0bd5c2c5` ON `django_store_record` (`parameters_id`);
CREATE INDEX `django_store_record_f6fca3bc` ON `django_store_record` (`launch_mode_id`);
CREATE INDEX `django_store_record_7b63f6d6` ON `django_store_record` (`datastore_id`);
CREATE INDEX `django_store_record_4a587f7c` ON `django_store_record` (`input_datastore_id`);
CREATE INDEX `django_store_record_37952554` ON `django_store_record` (`project_id`);
CREATE INDEX `tagging_taggeditem_5659cca2` ON `tagging_taggeditem` (`tag_id`);
CREATE INDEX `tagging_taggeditem_37ef4eb4` ON `tagging_taggeditem` (`content_type_id`);
CREATE INDEX `tagging_taggeditem_846f0221` ON `tagging_taggeditem` (`object_id`);
