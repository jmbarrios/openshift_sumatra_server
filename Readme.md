# Sumatra Server on Openshift

This git repository help you to get up and running Sumatra Server with Django 1.6 on Openshift. You can have a sqlite or MySQL database connected to Django. 

## Deployment

Create a Python application and optionally add MySQL cartridge

    rhc create-app sumatraserver python-2.7
    # Optional add MySQL cartridge
    rhc cartridge-add mysql-5.5 --app sumatraserver

Add this upstream repository

    cd sumatraserver
    git remote add upstream -m master https://bitbucket.org/jmbarrios/openshift_sumatra_server.git
    git pull -s recursive -X theirs upstream master

Then push the repositroy upstream
    
    git push 

Here, the admin user name and password will be dsiplayed.

Then you can chekout your application at:

    http://sumatraserver-$youtnamespace.rhcloud.com

__INFO__: If you missed the admin username and password display. You can look for this in your application.

    rhc ssh
    cd $OPENSHIFT_DATA_DIR
    cat CREDENTIALS

You will see something like this

    Django application credentials:
        user: admin
        NHKM8QBpnYS

## Sumatra Server Information

The `urls` who are managed by [Sumatra Server](https://bitbucket.org/apdavison/sumatra_server/) are under `^projects`.

## Extra information

Based on [openshift/django-example](https://github.com/openshift/django-example).

More info for [Sumatra and Sumatra Server](http://neuralensemble.org/sumatra/).

-----
> Mantained by [Juan M. Barrios](mailto:juan.barrios(en)conabio.gob.mx)