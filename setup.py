from setuptools import setup

packages = [
    'Django >= 1.6',
    'Sumatra',
    # 'Sumatra-server',
    'django-registration',
    'docutils',
    'httplib2',
    'parameters',
    'static3',
    'django-piston',
    'django-tagging',
    'South',
]

# links = [
#     'hg+https://bitbucket.org/apdavison/sumatra_server#egg=sumatra_server',
# ]

setup(name='smt-server-openshift',
      version='0.1dev',
      description='Openshift skeleton to deploy a Sumatra server',
      author='Juan M. Barrios',
      author_email='juan.barrios@conabio.gob.mx',
      url='http://neuralensemble.org/sumatra',
      # install_requires=['Django>=1.3'],
      install_requires=packages,
      # dependency_links=links,
      )
